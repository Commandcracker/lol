# Installation
```console
# clone the repo
$ git clone https://gitlab.com/Commandcracker/lol.git

# change the working directory to lol
$ cd lol

# install the requirements
$ python3 -m pip install -r requirements.txt
```
