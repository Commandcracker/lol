from requests import get


def get_json(url):
    return get(url).json()


class API(object):
    def __init__(self):
        self.version = get_json("http://ddragon.leagueoflegends.com/api/versions.json")[0]

    def get_champion_img(self, champ):
        return get("http://ddragon.leagueoflegends.com/cdn/" + self.version + "/img/champion/" + champ + ".png").content

    def get_champions(self):
        champions = []
        for champ in get_json("http://ddragon.leagueoflegends.com/cdn/" + self.version + "/data/en_US/champion.json")["data"]:
            champions.append(champ)
        return champions

