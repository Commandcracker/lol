#!/usr/bin/env python
# coding: utf-8

import threading
import os
import pyautogui
import time
import cv2
import numpy
import sys
from pynput.mouse import Controller as MouseController
from pynput.mouse import Button as MouseButton
from pynput.keyboard import Controller as KeyboardController
from windowcapture import WindowCapture
from api import API
from PyQt5 import (
    QtCore,
    QtGui,
    QtWidgets,
    uic
)
from PIL import ImageGrab

# Change the working directory to the folder this script is in.
# Doing this because I'll be putting the files from each video in their own folder on GitHub
os.chdir(os.path.dirname(os.path.abspath(__file__)))

# initialize the WindowCapture class
wincap = WindowCapture('League of Legends')
api = API()

mouse = MouseController()
keyboard = KeyboardController()

debug = None
accept = None
face_check = None
find_match = None
play = None
ban = None


def click(x, y, lol=True):
    old = mouse.position
    if lol:
        mouse.position = wincap.get_screen_position([x, y])
    else:
        mouse.position = [x, y]
    mouse.press(MouseButton.left)
    mouse.release(MouseButton.left)
    mouse.position = old


def find(image, screen, boolean, press=True, lol=True):
    if boolean:
        result = pyautogui.locate(image, screen, grayscale=True, confidence=0.8)
        if result is not None:
            if debug:
                img = cv2.rectangle(
                    numpy.array(screen),  # img
                    (result.left, result.top),  # pt1
                    (result.left + result.width, result.top + result.height),  # pt2
                    (0, 0, 255),  # Color
                    2  # Thickness
                )
                scale_percent = 60  # percent of original size
                width = int(img.shape[1] * scale_percent / 100)
                height = int(img.shape[0] * scale_percent / 100)
                dim = (width, height)
                cv2.imshow("Debug", cv2.resize(img, dim))
                cv2.waitKey(1)
            if press:
                click(result.left, result.top, lol)
            time.sleep(1)
            return result


class main(threading.Thread):
    def run(self):
        path = os.path.join(os.path.dirname(__file__), 'resources/assets')
        global play, ban, find_match, face_check, accept
        while True:
            screen = wincap.get_screenshot()
            if find(path + '/left.png', screen, True, False):
                self.run()
                return
            find(path + '/find_match.png', screen, find_match)
            find(path + '/accept.png', screen, accept)
            result = find(path + '/chemp_select.png', screen, True, False)
            if result:
                break
            time.sleep(0.5)

        while True:
            screen = wincap.get_screenshot()
            if find(path + '/left.png', screen, True, False):
                main()
                return
            if find(path + '/search.png', screen, True):
                keyboard.type(play)
                time.sleep(2)
                click(result.left, result.top + 50)
                time.sleep(1)
                break
            time.sleep(0.5)

        while True:
            screen = wincap.get_screenshot()
            if find(path + '/left.png', screen, True, False):
                self.run()
                return
            if find(path + '/lock_in.png', screen, True):
                break
            if find(path + '/search.png', screen, True):
                time.sleep(17)
                keyboard.type(ban)
                time.sleep(2)
                click(result.left, result.top + 50)
                time.sleep(1)
            find(path + '/ban.png', screen, True)
            time.sleep(0.5)

        if face_check:
            while True:
                screen = ImageGrab.grab()
                if find(path + '/left.png', screen, True, False):
                    self.run()
                    return
                find(path + '/set.png', screen, face_check, True, False)
                if find(path + '/push_all.png', screen, face_check, True, False):
                    exit()
                time.sleep(0.5)


def ban_comboBox_change(inp):
    global ban
    print("ban:", inp)
    ban = inp


def play_comboBox_change(inp):
    global play
    print("play:", inp)
    play = inp


def debug_checkBox_change(inp):
    global debug
    print("debug:", inp)
    debug = inp


def accept_checkBox_change(inp):
    global accept
    print("accept:", inp)
    accept = inp


def find_match_checkBox_change(inp):
    global find_match
    print("find_match:", inp)
    find_match = inp


def face_check_checkBox_change(inp):
    global face_check
    print("face_check:", inp)
    face_check = inp


class Ui(QtWidgets.QMainWindow):
    def __init__(self):
        super(Ui, self).__init__()
        uic.loadUi(os.path.join(os.path.dirname(__file__), 'resources/main.ui'), self)

        for champ in api.get_champions():
            r = api.get_champion_img(champ)
            pm = QtGui.QPixmap()
            pm.loadFromData(r)
            i = QtGui.QIcon()
            i.addPixmap(pm)
            self.play_comboBox.addItem(i, champ)
            self.ban_comboBox.addItem(i, champ)

        self.play_comboBox.activated[str].connect(play_comboBox_change)
        self.ban_comboBox.activated[str].connect(ban_comboBox_change)
        self.debug_checkBox.stateChanged.connect(lambda: debug_checkBox_change(self.debug_checkBox.isChecked()))
        self.accept_checkBox.stateChanged.connect(lambda: accept_checkBox_change(self.accept_checkBox.isChecked()))
        self.find_match_checkBox.stateChanged.connect(
            lambda: find_match_checkBox_change(self.find_match_checkBox.isChecked()))
        self.face_check_checkBox.stateChanged.connect(
            lambda: face_check_checkBox_change(self.face_check_checkBox.isChecked()))
        self.show()


if __name__ == "__main__":
    main().start()
    # run PyQt5 menu #
    app = QtWidgets.QApplication(sys.argv)
    # Theam
    app.setStyle("Fusion")
    palette = QtGui.QPalette()
    palette.setColor(QtGui.QPalette.Window, QtGui.QColor(53, 53, 53))
    palette.setColor(QtGui.QPalette.WindowText, QtCore.Qt.white)
    palette.setColor(QtGui.QPalette.Base, QtGui.QColor(25, 25, 25))
    palette.setColor(QtGui.QPalette.AlternateBase, QtGui.QColor(53, 53, 53))
    palette.setColor(QtGui.QPalette.ToolTipBase, QtCore.Qt.white)
    palette.setColor(QtGui.QPalette.ToolTipText, QtCore.Qt.white)
    palette.setColor(QtGui.QPalette.Text, QtCore.Qt.white)
    palette.setColor(QtGui.QPalette.Button, QtGui.QColor(53, 53, 53))
    palette.setColor(QtGui.QPalette.ButtonText, QtCore.Qt.white)
    palette.setColor(QtGui.QPalette.BrightText, QtCore.Qt.red)
    palette.setColor(QtGui.QPalette.Link, QtGui.QColor(42, 130, 218))
    palette.setColor(QtGui.QPalette.Highlight, QtGui.QColor(42, 130, 218))
    palette.setColor(QtGui.QPalette.HighlightedText, QtCore.Qt.black)
    app.setPalette(palette)
    # Name
    # app.setApplicationName("Youtube_Downloader")
    # Show main window
    window = Ui()
    app.exec_()
