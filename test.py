#!/usr/bin/env python3
from urllib3 import disable_warnings
from requests import get
import time
import pprint

disable_warnings()
while True:
    try:
        # url = "https://127.0.0.1:2997/lol-lobby-team-builder/champ-select/v1/current-champion"
        url = "https://127.0.0.1:2999/liveclientdata/allgamedata"
        r = get(url, verify=False)
        pprint.pprint(r.json())
    except:
        pass
    time.sleep(5)
